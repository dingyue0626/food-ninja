'use strict';

/**
 * @ngdoc overview
 * @name foodNinjaApp
 * @description
 * # foodNinjaApp
 *
 * Main module of the application.
 */
angular
  .module('foodNinjaApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
